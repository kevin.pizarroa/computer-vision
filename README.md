# Computer Vision

Computer Vision repository about the project: Emotions Recognition of Patients in Telemedicine, Based on Face
Detection.

This repository is organized under the next structure.

- models
- Papers
- scripts
- Emotions Recognition of Patients in Telemedicine, Based on Face Detection.pdf
- Plan.xlsx
- README.md

The "models" folder contains the models that results of training the CNN for emotion detection (InceptionV3 and ResNet50), the "Papers" folders contains some of the papers that my work is based on, then the "scripts" folder contains ResNet50, InceptionV3, Viola-Jones face detector and MTCNN face detector algorithms. Last but not least, the planification, the report and this README.